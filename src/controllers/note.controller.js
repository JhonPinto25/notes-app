const Note = require('../models/Note');
const passport = require('../config/passport');

noteCtrl = {

};

noteCtrl.renderNoteForm = async(req, res) => {
    res.render('notes/new-note');
};


noteCtrl.createNewNote = async(req, res) => {
    const { title, description } = req.body;
    const newNote = new Note({ title, description });

    newNote.user = req.user.id;
    await newNote.save();
    req.flash('sus-msj', 'Note added sucessfully');

    res.redirect('/notes');
};


noteCtrl.renderAllNote = async(req, res) => {

    const notes = await Note.find({ user: req.user.id });
    res.render('notes/all-notes', { notes });

};
noteCtrl.renderEditNote = async(req, res) => {
    const note = await Note.findById(req.params.id);
    if (note.user != req.user.id) {
        req.flash('error_msg', 'Not Authorized');
        return res.redirect('/notes');

    }
    res.render('notes/edit-note', { note });
};
noteCtrl.renderUpdateNote = async(req, res) => {
    const { title, description } = req.body;
    await Note.findByIdAndUpdate(req.params.id, { title, description });
    req.flash('sus-msj', 'Note updated sucessfully');
    res.redirect('/notes');
};

noteCtrl.renderDeleteNote = async(req, res) => {

    const idd = req.params.id;
    await Note.findByIdAndDelete(idd);
    req.flash('sus-msj', 'Note deleted sucessfully');
    res.redirect('/notes');
};

module.exports = noteCtrl;