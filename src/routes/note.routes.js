const { Router } = require('express');
const router = Router();

const {
    renderNoteForm,
    createNewNote,
    renderAllNote,
    renderEditNote,
    renderUpdateNote,
    renderDeleteNote
} = require('../controllers/note.controller');

const { isAuthenticated } = require('../helpers/auth');

// new notes
router.get('/notes/add', isAuthenticated, renderNoteForm);

router.post('/notes/new-note', isAuthenticated, createNewNote);

//get all notes
router.get('/notes', isAuthenticated, renderAllNote);

//Edit notes
router.get('/notes/edit/:id', isAuthenticated, renderEditNote);
router.put('/notes/edit/:id', isAuthenticated, renderUpdateNote);

//Delete note
router.delete('/notes/delete/:id', isAuthenticated, renderDeleteNote);




module.exports = router;