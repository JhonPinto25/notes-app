const usersCtrl = {

};

const User = require('../models/User');
const passport = require('passport');

usersCtrl.renderSignUpForm = (req, res) => {
    res.render('users/signup');
};

usersCtrl.signUp = async(req, res) => {
    const errors = [];

    const {
        name,
        lastname,
        email,
        password,
        confirm_password

    } = req.body;

    if (password != confirm_password) {
        errors.push({
            text: 'Password do not match'
        });

    }

    if (password.length < 5) {
        errors.push({
            text: 'Password must be at least 5 characters.'
        });

    }

    if (errors.length > 0) {

        res.render('users/signup', {
            errors,
            name,
            lastname,
            email
        });

    } else {
        const emailUser = await User.findOne({ email: email });
        if (emailUser) {

            req.flash('error_msg', 'The email is already in use');

            res.redirect('/users/signup');




        } else {
            const newUser = new User({
                name,
                lastname,
                email,
                password,
                confirm_password
            });

            newUser.password = await newUser.encryptPassword(password);
            await newUser.save();
            req.flash('succes_msj', 'You are registered');
            res.redirect('/users/signin');
        }
    }


};

usersCtrl.renderSignInForm = (req, res) => {
    res.render('users/signin');
};

usersCtrl.signIn = passport.authenticate('local', {
    failureRedirect: '/users/signin',
    successRedirect: '/notes',
    failureFlash: true
});


usersCtrl.logout = (req, res) => {
    req.logout();
    req.flash('succes_msj', 'You are logged out now');
    res.redirect('/users/signin');
};

module.exports = usersCtrl;